
class Fixnum
  ONES = {0 =>"zero", 1=>"one", 2=>"two", 3=>"three", 4=>"four", 5=>"five",
  6=>"six", 7=>"seven", 8=>"eight", 9=>"nine"}
  TENS = {10=>"ten", 20=>"twenty", 30 =>"thirty", 40=>"forty", 50=>"fifty", 60=>"sixty",
  70=>"seventy", 80=>"eighty", 90=>"ninety"}
  TEENS = {11=>"eleven", 12=> "twelve", 13=>"thirteen", 14=>"fourteen", 15=>"fifteen",
  16=>"sixteen", 17=>"seventeen", 18=>"eighteen", 19=>"nineteen"}
  MAGNITUDES = {
    100 => "hundred",
    1000 => "thousand",
    1000000 => "million",
    1000000000 => "billion",
    1000000000000 => "trillion"
  }

  # def to_arr
  #   self.to_s.split("").map{|n| n.to_i}
  # end

  def find_magnitude
    curr = []
    MAGNITUDES.select {|k,v| curr << k if k <= self}
    curr.last
  end

  def in_words
    if self < 10
      ONES[self]
    elsif self > 10 && self < 20
      TEENS[self]
    elsif self < 100
      if self % 10 == 0
        TENS[self]
      else
        TENS[(self.to_s[0] + "0").to_i] + " " + ONES[self.to_s[-1].to_i]
      end
    else
      magnitude = find_magnitude
      magnitude_words = (self / magnitude).in_words + " " + MAGNITUDES[magnitude]
      if (self % magnitude) != 0
        magnitude_words + " " + (self % magnitude).in_words
      else
        magnitude_words
      end
    end
  end
end
